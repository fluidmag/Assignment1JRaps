/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bankroll;

import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author cha_xi
 */
public class Bankroll {
    private double money;
    public Bankroll(){
        money = 0;
    }

    public Bankroll(double moneyInitial){
        money = moneyInitial;
    }
    public double getMoney() {
        return money;
    }
    /** add money to account
     * @param moneyAdd 
     */
    public void add(double moneyAdd){
        if(moneyAdd>=0)
        {
            money += moneyAdd;
        }
        else
        {
            System.out.println("Warning! add negative amount"
                    + " in Bankroll.add()");
        }
    }
    /**
     * subtract money to account
     * @param moneySubtract
    */
    public void subtract(double moneySubtract){
        if(moneySubtract>=0)
        {
            money -= moneySubtract;
        }
        else
        {
            System.out.println("Warning! substract negative amount"
                    + " in Bankroll.subtract");
        }
    }
    /**
     * change account, subtraction or addition
     * @param moneyChange 
     */
    public void change(double moneyChange){
        money +=moneyChange;
    }
    public String getFormattedMoney(){
       return NumberFormat.getInstance(Locale.CANADA_FRENCH).format(money);     
    }
    
    public void set(double amount)
    {
        money = amount;
    }
}
